from django.db import models
from webapps.modules.tools.models import VBaseModel, Choice, BaseInfoModel
from webapps.utils.util_box import gen_tuple_from_dict, get_section
from mptt.models import MPTTModel, TreeForeignKey

"""
    Model to display an Technology field. It inherit from VBaseModel and BaseInfoModel abstract classes
    :name: technology name.
    :website: technology's website.
    :description: technology's description.
"""
class Technology(MPTTModel,BaseInfoModel,VBaseModel):
    website = models.URLField(max_length=512,blank=True, null= False)
    technology_type = models.ForeignKey(Choice, null=True, blank=True,
                               help_text='Choose a technology type ie: mobile, OS',
                               limit_choices_to={'section': get_section('technologies-type')},
                               related_name='technology_type', )
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    
    class Meta:
        verbose_name_plural='Technologies'


