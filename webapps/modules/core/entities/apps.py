from django.apps import AppConfig


class EntitiesConfig(AppConfig):
    name = 'webapps.modules.core.entities'

    def ready(self):
        from . models import Entity
        from django.contrib.auth.models import User

        def get_profile(user):
            email = user.email or user.username
            name = (email.split('@')[0] if '@' in email else email) 
            entity, created = Entity.objects.get_or_create(
                                user=user,
                                defaults={'name': name,
                                          'email': email,
                                          'created_by': user,
                                           'modified_by': user})
           
            return entity
        User.profile = property(get_profile)  








