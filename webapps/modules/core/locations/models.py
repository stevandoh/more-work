from django.db import models
from django.utils.translation import gettext_lazy as _ 

from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager
from webapps.utils.util_box import gen_tuple_from_dict, get_section
from webapps.modules.tools.models import VBaseModel, Choice, BaseInfoModel
from webapps.modules.tools.managers import CoreManager

class Location(MPTTModel, VBaseModel): 
    name = models.CharField(max_length=250) 
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    type = models.ForeignKey(Choice, 
                               limit_choices_to={'section': get_section('locations.location-type')},
                               related_name='%(app_label)s%(class)s_type', )
    is_default = models.BooleanField(_('Default'), default=False)  
    sort = models.PositiveIntegerField(default=1000)

    iso2 = models.CharField(max_length=10, null=True, blank=True) 
    iso3 = models.CharField(max_length=10, null=True, blank=True) 
    alias = models.CharField(max_length=128, null=True, blank=True) 
    latitude = models.CharField(max_length=128, null=True, blank=True)
    longitude = models.CharField(max_length=128, null=True, blank=True)
    zip_code = models.CharField(max_length=10, null=True, blank=True) 
    currency_code = models.CharField(max_length=10, null=True, blank=True) 
    objects = CoreManager()

    tree = TreeManager() 


    def __str__(self):
        return "hello" 
        # if self.alias:
        #     return self.alias.encode('utf-8')
        # if self.alias and self.alias != self.name:
        #     return '%s (%s)' % (self.name.encode('utf-8'), self.alias.encode('utf-8'))
        # return self.name.encode('utf-8')

    class Meta:
        ordering = ('alias', 'name',) 
        unique_together = ('name', 'type', 'parent',)

    class MPTTMeta:
        order_insertion_by = ['sort']

    def save(self, **kwargs):
        if self.is_default:
            try:
                default_location = Location.objects.get(is_default=True)
            except self.DoesNotExist:
                pass
            else:
                default_location.is_default = False
                default_location.save()
        super(Location, self).save(**kwargs)   