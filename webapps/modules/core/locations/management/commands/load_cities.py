import json
import os
import csv
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.management.base import CommandError, BaseCommand
from webapps.modules.core.locations.models import Location
from webapps.modules.tools.models import Choice
from webapps.utils.util_box import gen_tuple_from_dict, get_section



class Command(BaseCommand):
    help = "Create all countries. "
    args = "<code code code code...>"

    def handle(self, *args, **options):
        rel_path = "./cities.csv"
        abs_file_path = os.path.join(os.path.dirname(__file__), rel_path)

        system_user = Location.get_system_user() 

        type_city = Choice.get_choice( 'city', get_section('locations.location-type'))
        type_region = Choice.get_choice( 'region', get_section('locations.location-type'))
        type_country = Choice.get_choice( 'country', get_section('locations.location-type'))

        # Location.tree.rebuild()
        with open(abs_file_path) as f:
            reader = csv.reader(f)
            cnt = 0
            for parts in reader:
                if cnt > 0: 
                    geoname_id = parts[0]
                    continent_code = parts[1]
                    continent_name = parts[2]
                    country_iso_code = parts[3]
                    country_name = parts[4]
                    subdivision_iso_code = parts[5]
                    subdivision_name = parts[6]
                    city_name = str(parts[7])
                    metro_code = parts[8]
                    time_zone = parts[9]

                    # if country_iso_code in ['GH']:

                    try:
                        country = Location.objects.get(iso2=country_iso_code, 
                                                       type=type_country)
                    except:
                        country = None

                    if country and city_name:
                        region = None
                        if subdivision_name:
                            region = Location.objects.get_or_create(
                                    name=subdivision_name,
                                    type=type_region,
                                    parent=country,
                                    defaults={
                                        'iso2': subdivision_iso_code[:2],
                                        'created_by': get_system_user, 
                                        'modified_by': get_system_user})[0]

                        # Location.objects.get_or_create(
                        #                 name=city_name,
                        #                 type=type_city,
                        #                 parent=region or country,
                        #                 defaults={
                        #                     'created_by': system_user,
                        #                     'modified_by': system_user,
                        #                     }) 
                    if cnt % 1000 == 0:
                        print ("Saving %s: %s" % (cnt, city_name) )
                cnt += 1

# Country,City,AccentCity,Region,Population,Latitude,Longitude