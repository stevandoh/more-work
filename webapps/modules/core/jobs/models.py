from django.db import models
from datetime import datetime, timedelta
from webapps.modules.tools.models import VBaseModel, Choice, BaseInfoModel
from webapps.utils.util_box import gen_tuple_from_dict, get_section
from webapps.modules.core.entities.models import Entity
from webapps.modules.core.tech.models import Technology
from webapps.modules.core.locations.models import Location

 
class JobPost(BaseInfoModel,VBaseModel):
	area = models.CharField(max_length=128, null=False,blank=False)
	venue =  models.ForeignKey(Choice, null=True, blank=True,
							   help_text='on-site or remote',
							   limit_choices_to={'section': get_section('jobs.job-venue')},
							   related_name='job_venue', )
	type =  models.ForeignKey(Choice, null=True, blank=True,
							   help_text='fulltime, part-time or contract',
							   limit_choices_to={'section': get_section('jobs.job-type')},
							   related_name='job_type', )
	how_to_apply = models.TextField(blank=True, null=True)
	start_date = models.DateTimeField(null=True, blank=True)
	end_date = models.DateTimeField(null=True, blank=True)
	country = models.ForeignKey(Location, blank= True, null=True)
	entity = models.ForeignKey(Entity, null=False, blank=False)
	remote_id = models.CharField(max_length=100, null=True, blank=True)
	source = models.ForeignKey(Entity, related_name='Source', blank=True, null=True)
	
	def save(self, *args, **kwargs):
		if self.start_date is None :
			self.start_date =  datetime.now()
		if self.end_date is None:
			self.end_date = datetime.now() + timedelta(days=30)
		super(JobPost,self).save(*args, **kwargs)


class Compensation(VBaseModel):
	job_post = models.ForeignKey(JobPost)
	compensation_type = models.ForeignKey(Choice, null=True, blank=True,
										  help_text='shares,equity, etc',
										  limit_choices_to={'section':get_section('compensations.compensation-type')}) 
	compensation_from = models.DecimalField(max_digits=9, decimal_places=2, default=0.0, null=True, blank=True)
	compensation_to = models.DecimalField(max_digits=9, decimal_places=2, default=0.0, null=True, blank= True) 


class JobPostInterest(VBaseModel):
	job_post = models.ForeignKey(JobPost)
	technology = models.ForeignKey(Technology) 

	def __str(self):
		return "(%s %s)" % (self.job_post, self.technology)  