from django.contrib import admin
from .models import JobPost, Compensation
from webapps.modules.tools.admin import BaseAdmin


class CompensationInlineAdmin (admin.TabularInline):
    """Inline configuration for Django's admin on the Ingredient model."""
    model = Compensation


@admin.register(JobPost)
class JobPostAdmin(BaseAdmin):
    list_display = ('area','venue','type','country','entity','remote_id','source')
    list_filter = ('area','venue','type','country','entity','remote_id','source')
    search_fields = ('area','venue','type','country','entity','remote_id','source')  
    inlines = [CompensationInlineAdmin]
    extra = 2
