from django.db import models
from django.conf import settings
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager
from webapps.utils.util_box import gen_tuple_from_dict, get_section
from mptt.managers import TreeManager
from . managers import CoreManager

import uuid

class BaseModel(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_removed = models.DateTimeField(null=True, blank=True)
    date_active = models.DateTimeField(auto_now_add=True)

    objects = CoreManager()

    class Meta:
        abstract = True
        ordering = ['date_created']

class BaseInfoModel(models.Model):
    name = models.CharField(max_length=128,blank=False, null=False)
    description = models.TextField(blank=False, null=False)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class ContactBaseModel(models.Model):
    email = models.EmailField(max_length=128 , null=False, blank=False)
    phone = models.CharField(max_length=128, null=True, blank=True)
    address = models.CharField(max_length=256, null=True, blank=True)
    website = models.URLField(max_length=512, null=True,blank=True)

    class Meta:
        abstract = True

# interest wikk inherent
class TrackModel(BaseModel): 
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, editable=False,
                                 related_name='%(app_label)s%(class)s_created_by')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, editable=False,
                                    related_name='%(app_label)s%(class)s_modified_by')

    class Meta:
        abstract = True

    @staticmethod
    def get_system_user():
        from django.contrib.auth.models import User
    
        if User.objects.filter(username='system_user').exists():
            return User.objects.filter(username='system_user')[0]
        return User.objects.create_user(username="system_user",password="qwerty123")


class VBaseModel(TrackModel):
    """
        Generate human readable codes for models that users will interact with.
        This is NOT a replacement for Primary Keys, it is more of a convenient
        way of uniquely identifying objects especially when we are using pks.
    """
    mcode = models.CharField(('Ref code'), max_length=50, null=True, blank=True) 
    scode = models.CharField(('code'), max_length=50, null=True, blank=True,
                             help_text="Leave empty to auto-generate") 
    serial = models.BigIntegerField(default=0, editable=False)

    class Meta:
        abstract = True 
        
    def is_vmodel(self):
        return True

    def get_serial(self):
        return zerofy(self.serial, 6)


class TreeMixin(models.Model):  

    class Meta:
        abstract = True 

    @property
    def first_ancestor(self):
        qs = self.get_ancestors() 
        return (qs[0] if qs else self)  

    def get_attrs(self, attr_list):
        attrs = {}
        for x in attr_list: 
            attrs[x] = self.get_attr(x)
        return attrs
    
    def get_attr(self, attr, is_callable=False): 
        workspaces = self.get_ancestors(ascending=True, include_self=True) 
        for x in workspaces:
            obj = getattr(x, attr, None)
            val = (obj() if is_callable else obj)
            if val:
                return val   
        
    def save(self, *args, **kwargs):  
        super(TreeMixin, self).save(*args, **kwargs)  
        # TODO: find a better to rebuild
        type(self).tree.rebuild()   


class Choice(MPTTModel,VBaseModel,TreeMixin): 
    name = models.CharField(max_length=100)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    slug = models.SlugField(max_length=100, null=True,)
    sort = models.PositiveIntegerField(default=1000)
    section = models.CharField(max_length=100, choices=gen_tuple_from_dict()) 
    is_reserved = models.BooleanField(default=False) 
    objects = CoreManager()

    tree = TreeManager()
        
    def __str__(self): 
        return self.name
    
    class Meta:
        ordering = ('section', 'sort', 'name',)
        unique_together = [('name', 'parent', 'slug', 'section',),]  
        
    class MPTTMeta:
        order_insertion_by = ['sort']
        
    class Attr:
        list_display = [ 'name', 'parent', 'section', 'slug', 'sort']
        list_filter = [ 'section',] 
        view_full_details = False

    def get_section(self):
        return self.section

    @staticmethod
    def get_choice(name, section, create_in_ancestor=True, **kwargs):  
        user = Choice.get_system_user()
        name = kwargs.get('title', name)
        return Choice.objects.get_or_update(
                    slug=slugify(name), 
                    section=section, 
                    defaults={'name': name.title(),  
                              'created_by': user, 
                              'modified_by': user
                              })[0]

    # @staticmethod
    # def get_choice(name, section, create_in_ancestor=True, **kwargs):  

    #     name = kwargs.get('title', name)
    #     return Choice.objects.workspace_get_or_update(
    #                 workspace, 
    #                 slug=slugify(name), 
    #                 section=section, 
    #                 defaults={'name': name.title(),  
    #                           # 'created_by': user, 
    #                           # 'modified_by': user
    #                           })[0]
    #   