from rest_framework import serializers
from webapps.modules.core.tech.models import Technology
# from webapps.modules.api.v1.tools.custom_fields import GenderField, EntityTypeField, AgeRangeField


class TechnologySerializer(serializers.ModelSerializer):
	

	class Meta:
		model = Technology
		fields = '__all__'


# class InterestSerializer(serializers.ModelSerializer):

# 	class Meta:
# 		model = Interest
# 		fields = '__all__'