from rest_framework import viewsets
from  django_filters.rest_framework import DjangoFilterBackend
from webapps.modules.api.v1f.core.jobs.serializers import JobPostSerializer, CompensationSerializer
from webapps.modules.core.jobs.models import JobPost, 	Compensation
from django_filters.rest_framework import DjangoFilterBackend
from . filters import JobPostFilter

class JobPostViewSet(viewsets.ModelViewSet):

    queryset = JobPost.objects.all()
    serializer_class = JobPostSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = JobPostFilter


class CompensationViewSet(viewsets.ModelViewSet):

    queryset = Compensation.objects.all()
    serializer_class = CompensationSerializer
 
  