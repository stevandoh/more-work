from django.conf.urls import url, include
from rest_framework.documentation import include_docs_urls
# from allauth.account.views import ConfirmEmailView


API_TITLE = 'JOBAPP API'

API_DESCRIPTION = 'coming soon'

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^v1n/', include('webapps.modules.api.v1n.urls')),
    url(r'^v1f/', include('webapps.modules.api.v1f.urls')),
    # url(r'^rest-auth/registration/account-confirm-email/(?P<key>\w+)/$', ConfirmEmailView.as_view(), name="account_confirm_email"),
    # url(r'^rest-auth/', include('rest_auth.urls')),
    # url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r'^docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION))
    # url(r'^api/docs', schema_view),
]