from rest_framework import viewsets
from .serializers import ChoiceSerializer
from webapps.modules.tools.models import Choice
from django_filters.rest_framework import DjangoFilterBackend

class ChoiceViewSet(viewsets.ModelViewSet):
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('section',)
   