from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .. v1n.core.entities.views import EntityViewSet
from .. v1n.core.jobs.views import JobPostViewSet, CompensationViewSet
from .. v1n.tools.views import ChoiceViewSet
from .. v1n.core.tech.views import TechnologyViewSet
from .. v1n.core.locations.views import LocationViewSet


# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'entities/entity', EntityViewSet)
router.register(r'jobs/jobpost', JobPostViewSet)
router.register(r'tools/choice', ChoiceViewSet)
router.register(r'jobs/compensation', CompensationViewSet)
router.register(r'tech/technology', TechnologyViewSet)
router.register(r'locations/location', LocationViewSet)

urlpatterns = [
	url(r'^', include(router.urls)),   
]
