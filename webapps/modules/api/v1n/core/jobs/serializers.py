from rest_framework import serializers
from webapps.modules.core.jobs.models import JobPost, Compensation
from webapps.modules.api.v1n.core.entities.serializers import EntitySerializer
from webapps.modules.api.v1n.tools.serializers import ChoiceSerializer
from .filters import JobPostFilter


class JobPostSerializer(serializers.ModelSerializer):
	type = ChoiceSerializer()
	venue = ChoiceSerializer()
	date_created = serializers.DateTimeField(read_only=True)
	date_modified = serializers.DateTimeField(read_only=True)
	date_active = serializers.DateTimeField(read_only=True)
	entity = EntitySerializer()
	# compensation_type = ChoiceSerializer()

	class Meta:
		model = JobPost 
		fields =  ['id','url','name','description','area','venue','type','how_to_apply',
					'start_date','end_date','entity','date_created',
				 	'date_modified','date_removed','date_active','guid','created_by','modified_by']
		extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
		 				'modified_by': {'default': serializers.CurrentUserDefault()},}
	

class CompensationSerializer(serializers.ModelSerializer):
	JobPost = JobPostSerializer()

	class Meta:
		fields = '__all__'
		model = Compensation
		extra_kwargs = {'created_by': {'default': serializers.CurrentUserDefault()},
		 				'modified_by': {'default': serializers.CurrentUserDefault()},}
	