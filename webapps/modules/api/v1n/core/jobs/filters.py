import django_filters
from webapps.modules.core.jobs.models import JobPost

class JobPostFilter(django_filters.rest_framework.FilterSet):
	# compensation_from = django_filters.NumberFilter(name="compensation_from", lookup_expr='gte')
	# compensation_to = django_filters.NumberFilter(name="compensation_to", lookup_expr='lte')
	start_date = django_filters.DateFilter(name="start_date", lookup_expr = 'gte')
	end_date = django_filters.DateFilter(name="start_date", lookup_expr = 'lte')
	class Meta:
		model = JobPost
		fields = ['venue', 'type',
				  'start_date', 'end_date','entity','guid','source','remote_id']