from datetime import datetime
from .constants import SECTIONS
import re

def choose_year():
	YEAR_CHOICES = [(r,r) for r in range(1975, datetime.today().year+1)]
	# year = models.IntegerField(_('year'), max_length=4, choices=YEAR_CHOICES, default=datetime.datetime.now().year)
	return YEAR_CHOICES

def extract_username_from_email(email):
	s  = email
	# to extract username
	match = re.search('([\w.-]+)', s)
	# to extract username and domain
	# match = re.search('([\w.-]+)@([\w.-]+)', s)
	username = match.group(1)
	return username

def gen_tuple_from_dict(section_values=SECTIONS):
    keys = section_values.keys() 
    sorted_keys = list(keys)
    sorted_keys.sort() 
    return [(x, section_values[x]) for x in sorted_keys]  

def get_section(key, section_values=SECTIONS):
    section_values[key]
    return key
