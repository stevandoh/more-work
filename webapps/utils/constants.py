SECTIONS = {
    'config.attribute-type': 'Config Attribute Type',
    
    'entities.entity-gender': 'Gender',
    'entities.entity-title': 'Title',
    'entities.entity-category': 'Entity Category',
    'entities.entity-type': 'Entity Type',
    'entities.entity-age-range': 'Age Range',
    'entities.entity-profession': 'Entity Profession',
    'jobs.job-type': 'Job Type',
    'jobs.job-venue': 'Job Venue',
    'jobs.job-advert-type': 'Job Advert Type',
    'technologies-type': 'Technology Type',
    'locations.location-type' : 'Location Type',
    'compensations.compensation-type' : 'Compensation Type'
}

optional_str = 'optional'