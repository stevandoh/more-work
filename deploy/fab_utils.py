import os
from fabric.api import run, cd, local
from fabric.api import task
from fabric.contrib import files

from contrib.clients import ClientObj
from constants import CLIENT_SETTINGS


@task
def setup_ssh_pubkey_auth(client):
    c_obj = ClientObj( CLIENT_SETTINGS[client] )

    local_pub_key = os.path.join(os.path.expanduser("~"), '.ssh/id_rsa.pub')
    if not os.path.exists(local_pub_key):
        print ("SSH public key not found. Please generate with this command: ssh-keygen -t rsa")
    
    else:
        # Create ssh folder
        ssh_path = '%s/.ssh' % c_obj.user_home_path
        if files.exists(ssh_path):
            print ('SSH folder already exists. Skipping ...')
        else:
            run('mkdir -p %s' % ssh_path)

        # Create authorized_key file
        ssh_keys_path = '%s/authorized_keys' % ssh_path
        if files.exists(ssh_keys_path):
            print ('SSH authorized_keys already exists. Skipping ...')
        else:
            run('touch %s' % ssh_keys_path)  

        # Append local public key to authorized file on server
        with open(local_pub_key, 'r') as f: 
            files.append(ssh_keys_path, f.read()) 
        f.closed

        # Set permissions
        run('chmod 700 %s' % ssh_path)
        run('chmod 600 %s' % ssh_keys_path)





