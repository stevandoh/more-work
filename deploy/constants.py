
DEV_SETTINGS = {
    'PROJECT_HOME': '..'
}

CLIENT_SETTINGS = {

    'morework': {
        'HOST': 'stevandoh@stevandoh.webfactional.com',
        'REMOTE_ALIAS': 'webfactional',
        'USER_HOME_PATH': '/home/stevandoh', 
        'PYTHON_VERSION': '3.5.2',
        'LIBS_DIR_PATH': '',
        'PROJ_DIR_PATH': 'webapps/jobapp_web/myproject',
        'DJANGO_SETTINGS_PATH': 'conf.dev_settings'
    }
}