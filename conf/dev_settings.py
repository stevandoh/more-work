from . settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sc_jobapp',  
        'USER': 'sc_user_jobapp',
        'PASSWORD': '2&][k8F_',   
        'HOST': '127.0.0.1', 
        'PORT': '5432',           
    },   
}

# Allow all host headers
ALLOWED_HOSTS = ['*']
# Static asset configuration
import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

MEDIA_ROOT = '/home/stevandoh/webapps/jobapp_static/media'
MEDIA_URL = '/static/media/'
STATIC_ROOT = '/home/stevandoh/webapps/jobapp_static/'
STATIC_URL = '/static/'

DEBUG = True
