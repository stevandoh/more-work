import sys
from fabric.api import env

from deploy import fab_deploy as deploy
from deploy import fab_utils as utils


# TODO: find a better way of getting host
if ':' in sys.argv[1]:
    from deploy.constants import CLIENT_SETTINGS
    client_name = sys.argv[1].split(':')[1]
    host = CLIENT_SETTINGS[client_name]['HOST']

    env.hosts = [host]

